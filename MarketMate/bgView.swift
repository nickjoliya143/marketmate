//
//  bgView.swift
//  MarketMate
//
//  Created by Apple on 04/04/23.
//

import UIKit

class bgView: UIView {

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderWidth = 0.1
        self.layer.cornerRadius = 20
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

class productView: UIView{
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 10
        self.layer.borderWidth = 0.2
        self.layer.borderColor = #colorLiteral(red: 0.1840000004, green: 0.05900000036, blue: 0.3650000095, alpha: 1)
    }
}
