//
//  FireHelp.swift
//  MarketMate
//
//  Created by Apple on 03/04/23.
//

import Foundation
import UIKit
import Firebase




class FIRUsers : NSObject{
    
    //MARK: register
    
    class func RegisterUser(dict: dataResponse,Success : @escaping (String) -> Void,Fail : @escaping (String) -> Void){
        
        var ref: DatabaseReference!
        ref = Database.database().reference()
        
        guard let numberId = dict["num"] as? String, numberId.count != 0 else {
            Fail("Upload error")
            return
        }
        
        ref.child("Users").child(numberId).setValue(dict) { (error, snapshot) in
            
            if error != nil {
                Fail("Try Again...")
            }else{
                Success("Upload successfully.")
            }
        }
    }
    
    
    //MARK: login
    
    class func LoginData(dict : dataResponse,Success : @escaping (dataResponse) -> Void, failure: @escaping (String?) -> Void){
        var ref: DatabaseReference!
        ref = Database.database().reference()
        guard let numberID = dict["num"] as? String, numberID.count != 0 else {
            return
        }
        ref.child("Users").child(numberID).observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                // user is already in our database
                if let dictResponse = snapshot.value as? dataResponse{
                    if let password = dictResponse["pass"] as? String,let pass = dict["pass"] as? String, password == pass{
                       Success(dictResponse)
                    }else{
                        failure("passcode or mobile number invalid")                    }
                }else{
                    failure("Mobile number not register, please sign in  first.")
                }
            } else {
                // not in database
                failure("Mobile number not register, please sign in  first.")
            }
        }
    }
    
    class func setupDefaultUser(dict : dataResponse,Success : @escaping (dataResponse) -> Void, failure: @escaping (String?) -> Void){
        var ref: DatabaseReference!
        ref = Database.database().reference()
        guard let numberID = dict["num"] as? String, numberID.count != 0 else {
            return
        }
        ref.child("Users").child(numberID).observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                // user is already in our database
                if let dictResponse = snapshot.value as? dataResponse{
                    
                       Success(dictResponse)
                    
                }else{
                    failure("Mobile number not register, please sign in  first.")
                }
            } else {
                // not in database
                failure("Mobile number not register, please sign in  first.")
            }
        }
    }
    
    class func DeleteUser(num: String,Success : @escaping (String) -> Void,Fail : @escaping (String) -> Void){
        
        var ref: DatabaseReference!
        ref = Database.database().reference()
        let childRef = ref.child("Users").child(num)
        
        childRef.removeValue { error, _ in
            if let error = error {
                print("Failed to delete child node: \(error.localizedDescription)")
                Fail("Fail To Delete Account.")
            } else {
//                print("User node deleted successfully.")
                Success("Account Deleted Successfully.")
            }
        }
        
    }
    
}


//MARK: Image Data Upload

class FIRStorage : NSObject{
    
    class func storeInImage(img : UIImage,strImgID : String, Success : @escaping (URL) -> Void,Fail : @escaping (String) -> Void){
        
        let storage = Storage.storage()
        
        // Create a root reference
        let storageRef = storage.reference()
        

        // Data in memory
          var data = Data()
        if let d = img.pngData(){
             data = d
        }else{
            Fail("something wrong")
        }
      // Create a reference to the file you want to upload
        let riversRef = storageRef.child("\(strImgID).jpg")

        // Upload the file to the path "images/rivers.jpg"
        _ = riversRef.putData(data, metadata: nil) { (metadata, error) in
          
             
        
            
            guard let metadata = metadata else {
            // Uh-oh, an error occurred!
                Fail("Image size may be much more. please select other images")
              return
          }
          // Metadata contains file metadata such as size, content-type.
          let size = metadata.size
          // You can also access to download URL after upload.
            print(size)
            
          riversRef.downloadURL { (url, error) in
            if url != nil{
                
            }else{
                Fail("something wrong")
            }
            guard let downloadURL = url else {
              // Uh-oh, an error occurred!
                Fail("something wrong")
              return
            }
              print("Image url:-")
              print(downloadURL)
              
            Success(downloadURL)
          }
        }
    }
}

//MARK: DB manage Products

class FireManager : NSObject {
    
    var ref: DatabaseReference!
    
    //MARK: upload Product
    
    class func UploadProduct(dict : dataResponse, Success : @escaping (String) -> Void,failure : @escaping (String) -> Void){
        
        var ref: DatabaseReference!
        ref = Database.database().reference()
        
        guard let ids = dict["strID"] as? String, ids.count != 0 else {
            failure("Local Error Try To Restart App.")
            return
        }
        
        ref.child("Products").child(ids).setValue(dict) { (error, snapshot) in
            
            if error != nil {
                failure("Product Not Added To database Try After Some Time.")
            }else{
                Success("Upload successfully.")
            }
        }
    }
    
    //MARK: get data
    
    class func GetProductArr(Success : @escaping ([String: dataResponse]) -> Void, failure: @escaping (String?) -> Void){
        
        var ref: DatabaseReference!
        ref = Database.database().reference()
        
        ref.child("Products").observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                // user is already in our database
                if let dictResponse = snapshot.value as? [String: dataResponse]{
                    Success(dictResponse)
                }else{
                    failure("Data Load Fail.")
                }
            } else {
                // not in database
                
            }
        }
    }
    
    
}


//MARK: Add Buy product

class FIRBuy: NSObject {
    
    var ref: DatabaseReference!
    
    //MARK: upload Product
    
    class func AddToBuy(dict : dataResponse, Success : @escaping (String) -> Void,failure : @escaping (String) -> Void){
        
        var ref: DatabaseReference!
        ref = Database.database().reference()
        
        guard let ids = dict["strID"] as? String, ids.count != 0 else {
            failure("Local Error Try To Restart App.")
            return
        }
        guard let num = dict["num"] as? String, ids.count != 0 else {
            failure("Local Error Try To Restart App.")
            return
        }
        
        ref.child("Order_List").child(num).child(ids).setValue(dict) { (error, snapshot) in
            
            if error != nil {
                failure("Product Not Added To database Try After Some Time.")
            }else{
                Success("Upload successfully.")
            }
        }
    }
}
