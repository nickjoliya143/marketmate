//
//  ProfileVC.swift
//  MarketMate
//
//  Created by Apple on 05/04/23.
//

import UIKit

class ProfileVC: UIViewController {

    
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        navigationController?.isToolbarHidden = false
//        navigationController?.isNavigationBarHidden = false
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isToolbarHidden = false
        navigationController?.isNavigationBarHidden = false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func btnDeleteAccount(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "Are you sure want to delete your account", message: "All data will be loss.!", preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "OK", style: .default, handler: { alert -> Void in
            
            FIRUsers.DeleteUser(num: CurruntUser["num"] as! String) { str in
                print(str)
                
                CurruntUser.removeAll()
                self.defaults.removeObject(forKey: "num")
                
                let alertController = UIAlertController(title: "Your Account Is Deleted Successfully", message: "", preferredStyle: .alert)
                let saveAction = UIAlertAction(title: "Login Again", style: .default, handler: { alert -> Void in
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
                    self.navigationController?.popViewController(animated: true)
                    self.navigationController?.pushViewController(vc , animated: true)
                })
                alertController.addAction(saveAction)
                self.present(alertController, animated: true, completion: nil)
                
                self.navigationController?.popViewController(animated: true)
                
                
            } Fail: { error in
                print("Task Fail..\(error)")
            }
            
        })
        alertController.addAction(saveAction)
        present(alertController, animated: true, completion: nil)
        
        
//        FIRUsers.DeleteUser(num: CurruntUser["num"] as! String) { str in
//            print(str)
//            self.navigationController?.popViewController(animated: true)
//        } Fail: { error in
//            print("Task Fail..\(error)")
//        }

    }
    

}
