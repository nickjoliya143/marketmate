//
//  ProductDesc.swift
//  MarketMate
//
//  Created by Apple on 04/04/23.
//

import UIKit
import Kingfisher

class ProductDesc: UIViewController {

    @IBOutlet weak var imgProduct: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblQuentity: UILabel!
    
    @IBOutlet weak var lblPrice: UILabel!
    
    @IBOutlet weak var lblQueToBuy: UILabel!
  
    @IBOutlet weak var lblFinalPrice: UILabel!
    
    @IBOutlet weak var tblReviewList: UITableView!
    
    @IBOutlet weak var lblOwnerInfo: UILabel!
    
    var price = 0
    var Quntity = 1{
        didSet{
            lblQueToBuy.text = "\(Quntity)"
            finalPrice  = Quntity * price
        }
    }
    var finalPrice = 0{
        didSet{
            lblFinalPrice.text = "\(finalPrice) $"
        }
    }
    
    var index = 0
    
    var url:URL? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        url = URL(string: arrProducts[index]["url"] as! String)
        imgProduct.kf.setImage(with: url)
        lblName.text = arrProducts[index]["name"] as? String
        lblQuentity.text = arrProducts[index]["qui"] as? String
        lblPrice.text = "\(arrProducts[index]["pri"] as? String ?? "Price in") $"
        lblOwnerInfo.text = """
        name:- \(arrProducts[index]["own"] as? String ?? "Owner Name Here")
        number:- \(arrProducts[index]["own_num"] as? String ?? "Owner Number Here")
"""
        price = convertToInt(arrProducts[index]["pri"]as! String) ?? 0
        lblFinalPrice.text = "\(price) $"
        if price == 0{
            showAlertWithInput(str: "Price Fetch Error.")
            navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func btnRemoveQue(_ sender: UIButton) {
        guard Quntity > 1 else{ return }
        Quntity  -= 1
    }
    
    @IBAction func btnAddQue(_ sender: UIButton) {
        guard Quntity < 10 else{ return }
        Quntity  += 1
    }
    
    @IBAction func btnContinueOrder(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "CodVC")as! CodVC
        vc.url = url
        vc.finalPrice = finalPrice
        vc.index = index
        vc.qui = Quntity
        navigationController?.pushViewController(vc, animated: true)
    }
    

    //MARK: convert to int
    
    func convertToInt(_ str: String) -> Int? {
        return Int(str)
    }
    
    //MARK: alert
    
    func showAlertWithInput(str: String){
        let alertController = UIAlertController(title: str, message: "", preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "OK", style: .default, handler: { alert -> Void in
        })
        alertController.addAction(saveAction)
        present(alertController, animated: true, completion: nil)
    }
}
