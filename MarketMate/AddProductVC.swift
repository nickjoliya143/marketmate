//
//  AddProductVC.swift
//  MarketMate
//
//  Created by Apple on 04/04/23.
//

import UIKit
import AVFoundation

class AddProductVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var btnTakePicture: UIButton!
    @IBOutlet weak var btnSelectImage: UIButton!
    @IBOutlet weak var fieldName: UITextField!
    @IBOutlet weak var fieldDescr: UITextField!
    @IBOutlet weak var fieldQuantity: UITextField!
    @IBOutlet weak var fielPrice: UITextField!
    let imagePicker = UIImagePickerController()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        
    }
    
    //MARK: Take Photo
    
    @IBAction func btnTakePicture(_ sender: UIButton) {
        if CurruntUser.count == 0{
            let vc = storyboard?.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
            navigationController?.pushViewController(vc, animated: true)
            return
        }
        if deviceHasCamera(){
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        }else{
            print("device has no camera installed")
            imagePicker.sourceType = .photoLibrary
            present(imagePicker, animated: true, completion: nil)
        }
        

    }
    
    //MARK: Choose Photo
    
    @IBAction func btnSelectImage(_ sender: UIButton) {
        if CurruntUser.count == 0{
            let vc = storyboard?.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
            navigationController?.pushViewController(vc, animated: true)
            return
        }
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    //MARK: add product in firebase
    
    @IBAction func btnAdd(_ sender: UIButton) {
        
        if CurruntUser.count == 0{
            let vc = storyboard?.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
            navigationController?.pushViewController(vc, animated: true)
            return
        }
        
        if isTextFieldEmpty(textField: fieldName){
            return
        }else if isTextFieldEmpty(textField: fieldDescr){
            return
        }else if isTextFieldEmpty(textField: fieldQuantity){
            return
        }else if isTextFieldEmpty(textField: fielPrice){
            return
        }
        
        let id = GetRandomString(length: 5)
        var imgUrl: URL? = nil
        
        
        //MARK: upload image
        
        FIRStorage.storeInImage(img: imgProduct.image ?? UIImage(named: "icon")!, strImgID: id) { url in
            imgUrl = url
            
            //MARK: upload all data
            
            
            let uData: dataResponse = [
                "strID": id,
                "name": self.fieldName.text ?? "Product Name",
                "desc": self.fieldDescr.text ?? "product description",
                "qui": self.fieldQuantity.text ?? "Product 100gm",
                "pri": self.fielPrice.text ?? "99.99",
                "own": CurruntUser["name"]as! String,
                "own_num": CurruntUser["num"]as! String,
                "url": imgUrl!.absoluteString
            ]
            
            FireManager.UploadProduct(dict: uData) { str in
                print(str)
                self.showAlertWithInput(str: "Upload Success", sub: str)
            } failure: { str in
                print(str)
                self.showAlertWithInput(str: "Product Upload Fail.", sub: str)
            }
            
            
        } Fail: { str in
            self.showAlertWithInput(str: "Image Upload To Database Error.", sub: str)
            return
        }

        self.navigationController?.popViewController(animated: true)
        self.navigationController?.popViewController(animated: true)

    }
    
    
    //MARK: picker code
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            return
        }
        imgProduct.image = image
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: has camera check
    
    func deviceHasCamera() -> Bool {
        let deviceTypes: [AVCaptureDevice.DeviceType] = [.builtInWideAngleCamera, .builtInTelephotoCamera, .builtInDualCamera, .builtInTrueDepthCamera]
        let discoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: deviceTypes, mediaType: AVMediaType.video, position: .unspecified)
        return !discoverySession.devices.isEmpty
    }

    //MARK: random string
    
    func GetRandomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let timestamp = String(Int(Date().timeIntervalSince1970))
        let randomString = String((0..<length).map{ _ in letters.randomElement()! })
        return "\(timestamp)_\(randomString)"
    }
    
    //MARK: isEmpty string check
    
    func isTextFieldEmpty(textField: UITextField) -> Bool {
        if let text = textField.text, !text.isEmpty {
            return false
        }
        return true
    }
    
    //MARK: Alert Show
    
    func showAlertWithInput(str: String, sub: String){
        let alertController = UIAlertController(title: str, message: sub, preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "OK", style: .default, handler: { alert -> Void in
        })
        alertController.addAction(saveAction)
        present(alertController, animated: true, completion: nil)
    }
//
//    func checkLogin() -> Bool{
//        if CurruntUser.count == 0{
//            return false
//        }
//        return true
//    }
}
