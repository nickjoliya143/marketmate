//
//  LoginVC.swift
//  MarketMate
//
//  Created by Apple on 03/04/23.
//

import UIKit

class LoginVC: UIViewController {
    
    let defaults = UserDefaults.standard

    @IBOutlet weak var fieldNNumber: UITextField!
    @IBOutlet weak var fieldPasscode: UITextField!
    var number: String = ""{
        didSet{
            fieldNNumber.text = number
            print(number)
        }
    }
    var passcode: String = ""{
        didSet{
            fieldPasscode.text = passcode
            print(passcode)
        }
    }
//    var isLogin: Bool = false{
//        didSet{
//            if isLogin{
//                self.dismiss(animated: true)
//            }
//        }
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        navigationController?.isToolbarHidden = true
//        navigationController?.isNavigationBarHidden = true
        let vc = storyboard?.instantiateViewController(withIdentifier: "GetStartedVC")as! GetStartedVC
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isToolbarHidden = true
        navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.isToolbarHidden = false
        navigationController?.isNavigationBarHidden = false
    }
    
    
    @IBAction func btnSkipLogin(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLogin(_ sender: UIButton) {
        if isTextFieldEmpty(textField: fieldNNumber){
            showAlertWithInput(str: "Enter Number.", isNumber: true)
            return
        }
        if isTextFieldEmpty(textField: fieldPasscode){
            showAlertWithInput(str: "Enter Passcode", isNumber: false)
            return
        }
        let uData: dataResponse = [
            "num": fieldNNumber.text ?? "name here!",
            "pass": fieldPasscode.text ?? "passcode here!"
        ]
        
        FIRUsers.LoginData(dict: uData) { response in
            #warning("set user data")
            CurruntUser = response
            self.defaults.set(CurruntUser["num"], forKey: "num")
            self.navigationController?.popViewController(animated: true)
            print(response)
            
        } failure: { error in
//            print(error!)
            let alertController = UIAlertController(title: error, message: "", preferredStyle: .alert)
            let saveAction = UIAlertAction(title: "OK", style: .default, handler: { alert -> Void in
            })
            alertController.addAction(saveAction)
            self.present(alertController, animated: true, completion: nil)
        }

    }
    
    @IBAction func btnSignin(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "RegisterVC")as! RegisterVC
//        vc.modalPresentationStyle = .fullScreen
//        present(vc, animated: true)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func showAlertWithInput(str: String, isNumber: Bool){
        let alertController = UIAlertController(title: str, message: "missing!", preferredStyle: .alert)
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter text"
        }
        
        let saveAction = UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
            let textField = alertController.textFields![0] as UITextField
//            print("Text entered: \(textField.text ?? "")")
            if isNumber{
                self.number = textField.text ?? ""
            }else{
                self.passcode = textField.text ?? ""
            }
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
        
    }

    func isTextFieldEmpty(textField: UITextField) -> Bool {
        if let text = textField.text, !text.isEmpty {
            return false
        }
        return true
    }
}
