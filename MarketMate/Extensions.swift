//
//  Extensions.swift
//  MarketMate
//
//  Created by Apple on 04/04/23.
//

import Foundation
import UIKit

extension UIButton{
    open override func awakeFromNib() {
        super.awakeFromNib()
//        self.layer.borderWidth = 0.4
        self.layer.cornerRadius = 20
    }
}
