//
//  ViewController.swift
//  MarketMate
//
//  Created by Apple on 03/04/23.
//

import UIKit
import Kingfisher

var CurruntUser: dataResponse = [:]
var arrProducts: [dataResponse] = []

class HomeVC: UIViewController {
    
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var cvSlideShow: UICollectionView!
    var timer: Timer?
    @IBOutlet weak var cvProducts: UICollectionView!
//#warning("set height regarding product count")
    @IBOutlet weak var constProCVHeight: NSLayoutConstraint!
    @IBOutlet weak var lblUser: UILabel!
    
    let arrSlider = ["g1", "g2", "g3"]
    
    //MARK: did load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cvSlideShow.delegate = self
        cvSlideShow.dataSource = self
        cvProducts.delegate = self
        cvProducts.dataSource = self
        cvSlideShow.layer.borderWidth = 0.4
        //        cvSlideShow.layer.cornerRadius = 10
        cvSlideShow.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        if let uNumber = defaults.string(forKey: "num") {
//#warning("set userdata.")
            print(uNumber)
            FIRUsers.setupDefaultUser(dict: ["num":uNumber]) { response in
                CurruntUser = response
                self.TempGret(str: "Welcome \(CurruntUser["name"] ?? "UserName")")
            } failure: { str in
//                print(str)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        } else {
            let vc = storyboard?.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
//        defaults.removeObject(forKey: "num")
    }
    
    //MARK: view will appear
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        
//        if let uNumber = defaults.string(forKey: "num") {
////#warning("set userdata.")
//            print(uNumber)
//            FIRUsers.setupDefaultUser(dict: ["num":uNumber]) { response in
//                CurruntUser = response
//                self.TempGret(str: "Welcome \(CurruntUser["name"] ?? "UserName")")
//            } failure: { str in
////                print(str)
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
//
//        } else {
//            let vc = storyboard?.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
        startSlidingCollectionView()
        
        //MARK: fetch all products
        
        FireManager.GetProductArr { response in
            arrProducts.removeAll()
            for i in response{
                arrProducts.append(i.value)
            }
            self.cvProducts.reloadData()
            self.setCvHeight()
        } failure: { str in
            
        }

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        stopSlidingCollectionView()
    }
    
    //MARK: set cv height
    
    func setCvHeight(){
        let pNum = arrProducts.count
        let rowNum = Int((pNum + 1)/2)
        constProCVHeight.constant = CGFloat(cvProducts.bounds.width/2) * CGFloat(rowNum)
        constProCVHeight.constant += 100
//        print(constProCVHeight.constant)
    }
    
    //MARK: start slider
    
    func startSlidingCollectionView() {
        guard timer == nil else { return }
        
        timer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: true) { [weak self] timer in
            guard let self = self else { return }
            
            var currentOffset = self.cvSlideShow.contentOffset
            
            currentOffset.x += self.cvSlideShow.bounds.width
            
            if currentOffset.x+100 > self.cvSlideShow.contentSize.width {
                currentOffset.x = 0
            }
            
            UIView.animate(withDuration: 0.5) {
                self.cvSlideShow.contentOffset = currentOffset
            }
        }
    }
    
    //MARK: stop slider
    
    func stopSlidingCollectionView() {
        timer?.invalidate()
        timer = nil
    }
    
    
    //MARK: set ProductCV height
    
    func setProductCVHeight(){
        
    }
    
    
    //MARK: temporary grating
    
    func TempGret(str: String){
        let originalText = lblUser.text
        lblUser.text = str
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.lblUser.text = originalText
        }
    }
    
    
    //MARK: Alert Function
    
    func showAlertWithInput(str: String, sub: String){
        let alertController = UIAlertController(title: str, message: sub, preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "OK", style: .default, handler: { alert -> Void in
        })
        alertController.addAction(saveAction)
        present(alertController, animated: true, completion: nil)
    }
}


//MARK: CV extentions

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == cvSlideShow{
            return 3
        }else if collectionView == cvProducts{
//#warning("set product count")
            return arrProducts.count
        }
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == cvSlideShow{
            let cell = cvSlideShow.dequeueReusableCell(withReuseIdentifier: "sliderCell", for: indexPath)as! sliderCell
            cell.img.image = UIImage(named: arrSlider[indexPath.row])
            cell.img.backgroundColor = #colorLiteral(red: 0.7799999714, green: 0.9139999747, blue: 0.6899999976, alpha: 1)
            return cell
        }else if collectionView == cvProducts{
            let cell = cvProducts.dequeueReusableCell(withReuseIdentifier: "productCell", for: indexPath)as! productCell
            cell.lblProTitle.text = arrProducts[indexPath.row]["name"] as? String
            cell.lblProDesc.text = """
\(arrProducts[indexPath.row]["qui"] as? String ?? "Product") at \(arrProducts[indexPath.row]["pri"] as? String ?? "Price")$.
"""
            let url = URL(string: arrProducts[indexPath.row]["url"] as? String ?? "https://imgs.search.brave.com/1nF4DG3fKI2V5nixfCVx_xEXoY2S0n9gcO_bnaaZyWs/rs:fit:711:225:1/g:ce/aHR0cHM6Ly90c2Ux/Lm1tLmJpbmcubmV0/L3RoP2lkPU9JUC5F/OWlQeVRMbGxuNjBI/eW5MQzRKQ0JRSGFF/OCZwaWQ9QXBp")
            cell.imgProduct.kf.setImage(with: url)
            
            return cell
        }
        let cell = cvSlideShow.dequeueReusableCell(withReuseIdentifier: "sliderCell", for: indexPath)as! sliderCell
        cell.img.image = UIImage(named: "icon")
        cell.img.backgroundColor = #colorLiteral(red: 0.7799999714, green: 0.9139999747, blue: 0.6899999976, alpha: 1)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == cvSlideShow{
            let sizer = CGSize(width: cvSlideShow.bounds.width, height: cvSlideShow.bounds.height)
            return sizer
        }else if collectionView == cvProducts{
            let sizer = CGSize(width: cvProducts.bounds.width/2, height: cvProducts.bounds.width/2)
            return sizer
        }
        let sizer = CGSize(width: cvSlideShow.bounds.width, height: cvSlideShow.bounds.height)
        return sizer
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == cvProducts{
            let vc = storyboard?.instantiateViewController(withIdentifier: "ProductDesc")as! ProductDesc
            vc.index = indexPath.row
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}


//MARK: slider cell

class sliderCell: UICollectionViewCell{
    
    @IBOutlet weak var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

//MARK: product cell

class productCell: UICollectionViewCell{
    
    @IBOutlet weak var lblProTitle: UILabel!
    @IBOutlet weak var lblProDesc: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
