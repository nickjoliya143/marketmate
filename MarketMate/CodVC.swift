//
//  CodVC.swift
//  MarketMate
//
//  Created by Apple on 04/04/23.
//

import UIKit
import Kingfisher

class CodVC: UIViewController {
    

    @IBOutlet weak var imgProduct: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var fielldAddress: UITextView!
    
    var index = 0
    
    var qui = 0
    
    var finalPrice = 0
    
    var url: URL? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        lblName.text = "\(arrProducts[index]["name"] ?? "Product Name") on \(finalPrice) $"
        imgProduct.kf.setImage(with: url)
    }

    @IBAction func btnBuy(_ sender: UIButton) {
        if CurruntUser.count == 0{
            let vc = storyboard?.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
            navigationController?.pushViewController(vc, animated: true)
            return
        }
        
        let str = GetRandomString(length: 5)
        let uData: dataResponse = [
            "num": arrProducts[index]["own_num"] ?? "Service provider number here",
            "ProName": arrProducts[index]["name"] ?? "Product name here",
            "ProStr": arrProducts[index]["strID"] ?? "Product ID here",
            "strID": str,
            "FPrice": "\(finalPrice)",
            "BuyerNum": CurruntUser["num"] ?? "Buyer Number here",
            "ProUrl": "\(url?.absoluteString ?? "https://imgs.search.brave.com/xeIv87owdT-wT50evUAyES8pzKt_4q6Qze_mrX8449o/rs:fit:881:225:1/g:ce/aHR0cHM6Ly90c2U0/Lm1tLmJpbmcubmV0/L3RoP2lkPU9JUC54/ZGR5TTVaNWxsd2U1/bnoweEFuaHZBSGFE/XyZwaWQ9QXBp")"
        ]
        FIRBuy.AddToBuy(dict: uData) { str in
            print(str)
        } failure: { error in
            print(error)
        }

        navigationController?.popViewController(animated: true)
        navigationController?.popViewController(animated: true)
    }
    
    func isTextFieldEmpty(textField: UITextField) -> Bool {
        if let text = textField.text, !text.isEmpty {
            return false
        }
        return true
    }
    
    func GetRandomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let timestamp = String(Int(Date().timeIntervalSince1970))
        let randomString = String((0..<length).map{ _ in letters.randomElement()! })
        return "\(timestamp)_\(randomString)"
    }
}
