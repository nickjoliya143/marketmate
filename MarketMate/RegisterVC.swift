//
//  RegisterVC.swift
//  MarketMate
//
//  Created by Apple on 03/04/23.
//

import UIKit

class RegisterVC: UIViewController {
    
    let defaults = UserDefaults.standard

    @IBOutlet weak var fieldName: UITextField!
    @IBOutlet weak var fieldNumber: UITextField!
    @IBOutlet weak var fieldPassword: UITextField!
    @IBOutlet weak var fieldConfirmPass: UITextField!
    
    var isLogin: Bool = false{
        didSet{
            if isLogin{
                self.dismiss(animated: true)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func btnSignUp(_ sender: UIButton) {
        if isTextFieldEmpty(textField: fieldName){
            showAlertWithInput(str: "Enter Name Please")
            return
        }
        if isTextFieldEmpty(textField: fieldNumber){
            showAlertWithInput(str: "Enter Number Please")
            return
        }
        if isTextFieldEmpty(textField: fieldPassword){
            showAlertWithInput(str: "Enter Passcode")
            return
        }
        if fieldPassword.text != fieldConfirmPass.text{
            showAlertWithInput(str: "Confirm Passcode is not same as Passcode")
            return
        }
//        #warning("register here")
        var uData: dataResponse = [:]
        uData["name"] = fieldName.text ?? "Name Here!"
        uData["num"] = fieldNumber.text ?? "Number Here!"
        uData["pass"] = fieldPassword.text ?? "Passcode Here!"
        
        FIRUsers.RegisterUser(dict: uData) { str in
            let alertController = UIAlertController(title: "Registration Success..", message: "", preferredStyle: .alert)
            let saveAction = UIAlertAction(title: "OK", style: .default, handler: { alert -> Void in
                self.defaults.set(self.fieldNumber.text ?? "numbers here", forKey: "num")
                self.navigationController?.popViewController(animated: true)
                self.navigationController?.popViewController(animated: true)
            })
            alertController.addAction(saveAction)
            self.present(alertController, animated: true, completion: nil)
            
        } Fail: { error in
            self.showAlertWithInput(str: error)
        }

    }
    
    @IBAction func btnHaveAccount(_ sender: UIButton) {
//        self.dismiss(animated: true)
        navigationController?.popViewController(animated: true)
    }

    
    
    func showAlertWithInput(str: String){
        let alertController = UIAlertController(title: str, message: "", preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "OK", style: .default, handler: { alert -> Void in
        })
        alertController.addAction(saveAction)
        present(alertController, animated: true, completion: nil)
    }
    func isTextFieldEmpty(textField: UITextField) -> Bool {
        if let text = textField.text, !text.isEmpty {
            return false
        }
        return true
    }
}
